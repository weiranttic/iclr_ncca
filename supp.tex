\documentclass{article}
\usepackage{iclr2016_conference,times}
\usepackage{hyperref}
\usepackage{url}
\usepackage{natbib}

\usepackage{algorithm}
\usepackage{algorithmic}
\usepackage{amsmath,amssymb,amsthm}
\usepackage{psfrag}
\usepackage{color}
\usepackage{pdfpages}
\usepackage{wrapfig}

\input{MACPdef}
\input{MACPdef-ams}
\graphicspath{{grf/}}

\DeclareMathOperator*{\argmax}{arg\,max}
\DeclareMathOperator*{\argmin}{arg\,min}
\DeclareMathOperator*{\corr}{corr}
\newcommand{\expx}[1]{\bbE\!\left[#1\right]}
\newcommand{\expy}[1]{\bbE\!\left[#1\right]}
\newcommand{\expxy}[1]{\bbE\!\left[#1\right]}
\newcommand{\expxi}[1]{\bbE[#1]} % inline
\newcommand{\expyi}[1]{\bbE[#1]} % inline
\newcommand{\expxyi}[1]{\bbE[#1]} % inline
\newcommand{\tracei}[1]{\ensuremath{\traceop(#1)}}%inline
\newcommand{\ie}{{\em i.e., }}
\newcommand{\eg}{{\em e.g., }}
% \newcommand{\expx}[1]{\bbE_x\left[#1\right]}
% \newcommand{\expy}[1]{\bbE_y\left[#1\right]}
% \newcommand{\expxy}[1]{\bbE_{xy}\left[#1\right]}
% \newcommand{\expxgy}[1]{\bbE_{x|y}\left[#1\right]}
% \newcommand{\expygx}[1]{\bbE_{y|x}\left[#1\right]}
% \newcommand{\probx}[1]{\bbP_x\left[#1\right]}
% \newcommand{\proby}[1]{\bbP_y\left[#1\right]}
% \newcommand{\probxy}[1]{\bbP_{xy}\left[#1\right]}
\newcommand{\exy}{\x_y}
% \setlength{\abovedisplayskip}{0.4\abovedisplayskip}
% \setlength{\belowdisplayskip}{0.3\belowdisplayskip}

\newcommand{\klcomment}[1]{\textcolor{red}{#1 (KL)}\ }
\newcommand{\wwcomment}[1]{\textcolor{magenta}{#1 (WW)}\ }
\newcommand{\tmcomment}[1]{\textcolor{blue}{#1 (TM)}\ }
\newcommand{\PLCCA}{PLCCA}
\newcommand{\NCCA}{NCCA}

\title{Supplementary materials for\\``Nonparametric Canonical Correlation Analysis''}

\author{Tomer Michaeli\\
  Technion--Israel Institute of Technology\\
  Haifa, Israel \\
  \texttt{tomer.m@technion.ac.il}\\
  \And
  Weiran Wang \& Karen Livescu\\
  TTI-Chicago\\
  Chicago, IL 60637, USA \\
  \texttt{\{weiranwang,klivescu\}@ttic.edu}
}

% The \author macro works with any number of authors. There are two commands
% used to separate the names and addresses of multiple authors: \And and \AND.
% 
% Using \And between authors leaves it to \LaTeX{} to determine where to break
% the lines. Using \AND forces a linebreak at that point. So, if \LaTeX{}
% puts 3 of 4 authors names on the first line, and the last on the second
% line, try using \AND instead of \And before the third author name.

\begin{document}
\maketitle

\appendix

\section{Proof of Lemma~3.1 of the main paper}
\label{sec:appendixConsEst}

\begin{lemma}[Lemma~3.1 of the main paper]
  For a fixed $f$, the function $g$ optimizing the objective
  \begin{align} \tag{*}
    \max_{g\in\calB} \;  \expxy{f(X)^\top g(Y)} \quad \text{s.t.} \quad  \expx{f(X)f(X)^\top}=\expy{g(Y)g(Y)^\top}=\I,
  \end{align}
  is given by
  \begin{align*}
    g(Y)=\left( \expy{ \expxi{f(X)|Y} \expxi{f(X)|Y}^\top } \right)^{-\frac{1}{2}} \expxi{f(X)|Y},
  \end{align*}
  assuming that the matrix $\expyi{\expxi{f(X)|Y} \expxi{f(X)|Y}^\top}$ is non-singular.
\end{lemma}
\begin{proof}
Let the eigen-decomposition of the second-order moment of $\expxi{f(X)|Y}$ be $\expyi{ \expxi{f(X)|Y}\expxi{f(X)|Y}^\top }=\A\D\A^\top$ and define $U=\A^\top \expxi{f(X)|Y}$ and $\tilde{g}(Y)=\A^\top g(Y)$. Then the objective $\expxyi{f(X)^\top g(Y)}$ can be written as
\begin{equation*}
  \expyi{\expxi{f(X)|Y}^\top g(Y)}=\expyi{(\A^\top\expxi{f(X)|Y})^\top (\A^\top g(Y))}=\expyi{U^\top\tilde{g}(Y)}.
\end{equation*}
Similarly, the constraint $\expyi{g(Y)g(Y)^\top}=\I$ can be expressed as
\begin{equation*}
  \expyi{\tilde{g}(Y) \A^\top\A\tilde{g}(Y)^\top} = \expyi{\tilde{g}(Y) \tilde{g}(Y)^\top} = \I.
\end{equation*}
Therefore, our optimization problem (*) can be written in terms of $\tilde{g}$ as
\begin{align*}
  \max_{\tilde{\g}}\;  \expy{U^\top \tilde{g}(Y)} \quad \text{s.t.} \quad  \expy{\tilde{g}(Y)\tilde{g}(Y)^\top}=\I. 
\end{align*}
Our objective is the sum of correlations in all $L$ dimensions. Let us consider the correlation in the $j$th dimension. From the Cauchy-Schwartz inequality, we have
\begin{align*}
  \expy{U_j \tilde{g}_j(Y)}\le \sqrt{ \expy{U_j^2} \expy{ \tilde{g}_j(Y)^2} }=\sqrt{ \expy{U_j^2}}
\end{align*}
with equality if and only if $\tilde{g}_{j}(Y)=c_j U_j$ for some scalar $c_j$ with probability 1. Note that choosing each $\tilde{g}_j(Y)$ to be proportional to $U_j$ is valid, since the dimensions of $U$ are uncorrelated (as $\expyi{UU^\top}=\A^\top \expy{\expxi{f(X)|Y}\expxi{f(X)|Y}^T} \A=\D$). In order for each $\tilde{g}_j(Y)$ to have unit second order moment, we must have $c_j=\left(\expyi{U_j^2}\right)^{-1/2}=D_{jj}^{-1/2}$. Therefore, $\tilde{g}(Y)=\D^{-1/2} U$ so that ${g}(Y) = \A \D^{-\frac{1}{2}} U = \A \D^{-\frac{1}{2}} \A^\top \expxi{f(X)|Y}  = (\expyi{ \expxi{f(X)|Y}\expxi{f(X)|Y}^\top })^{-1/2} \expxi{f(X)|Y}$, proving the lemma.
\end{proof}

\section{Sufficient condition for the compactness of $\K$}
Recall that the operator $\K$ is defined by $(\K f_{j})(\x)=\int k(\x,\x') f_j(\x') p(\x')d\x'$ with the kernel
\begin{align}\label{e:K-def}
  k(\x,\x')= \expy{S(\x,Y)S(\x',Y)},
\end{align}
where $S(\x,\y)=p(\x,\y)/(p(\x)p(\y))$.
The following lemma provides a sufficient condition for $\K$ to be compact (and thus to have a discrete set of eigenvalues and eigenfunctions).
\begin{lemma}
  The operator $\K$ is compact if
  \begin{align}\label{e:compactK}
    \expxy{ S(X,Y) } < \infty.
  \end{align}
\end{lemma}
\begin{proof}
  To ensure the compactness of $\K$, it is sufficient to require that $k(\x,\x')$ be a Hilbert-Schmidt kernel, namely that
  \begin{align*}
    \iint k^2(\x,\x') p(\x) p(\x') d \x d\x' < \infty.
  \end{align*}
  Applying the Cauchy-Schwarz inequality for expectation to the definition of $k(\x,\x')$, we obtain
  \begin{align*}
    \iint k^2(\x,\x')p(\x)p(x')d\x d\x' &\leq \iint \expy{S^2(\x,Y)}\expy{S^2(\x',Y)}d\x d\x' \nonumber\\
    & = \left(\int \expy{S^2(\x,Y)} d\x\right)^2 \nonumber\\
    & = \left(\iint \left(\frac{p(\x,\y)}{p(\x)p(\y)} \right)^2 p(\x) p(\y) d\x d\y\right)^2 \nonumber\\
    & = \left(\iint \left(\frac{p(\x,\y)}{p(\x)p(\y)} \right) p(\x,\y) d\x d\y\right)^2 \nonumber\\
    & = \left( \expxy{S(X,Y)} \right)^2.
  \end{align*}
  Therefore, $\K$ is compact if $\left( \expxy{S(X,Y)} \right)^2<\infty$, which is equivalent to the condition~\eqref{e:compactK}.
\end{proof}

\section{The function $f(\x)=1$ is an eigenfunction of the operator $\K$}
\begin{lemma}
The constant function $f(\x)=1$ is an eigenfunction of the operator $\K$ defined in \eqref{e:K-def} associated with eigenvalue $1$.
\end{lemma}
\begin{proof}
We have
\begin{align*}
  (\K f)(\x)&=\int k(\x,\x')f(\x') p(\x') d \x' \\
  &=\int k(\x,\x')  p(\x') d \x' \\
  &=\int \expy{ \left(\frac{p(\x,\y)}{p(\x)p(\y)}\right) \left( \frac{p(\x',\y)}{p(\x')p(\y)} \right) }  p(\x') d \x' \\
  &=\iint  \left(\frac{p(\x,\y)}{p(\x)p(\y)}\right) \left( \frac{p(\x',\y)}{p(\x')p(\y)} \right) p(\x') p(\y) d \x' d \y \\
  &=\iint  \left(\frac{p(\x,\y)}{p(\x)p(\y)}\right) p(\x',\y)  d \x' d \y \\
  &=\int  \left(\frac{p(\x,\y)}{p(\x)p(\y)}\right) p(\y) d \y  \\
  &=\frac{1}{p(\x)} \int  p(\x,\y)  d \y \\
  &=1.
\end{align*}
\end{proof}

%\small
%\bibliographystyle{iclr2016_conference}
%\bibliography{iclr16a}

\end{document}
