filename=main

pdf: $(filename).ps
	./llpdfus $(filename).ps
	atril $(filename).pdf &

$(filename).ps: $(filename).dvi
	dvips -Ppdf -t letter -G0 $(filename).dvi

$(filename).dvi: $(filename).tex intro.tex algorithm.tex related.tex experiments.tex conclusion.tex  iclr16a_abbrv.bib appendix.tex
	latex $(filename)
	bibtex --min-crossrefs=20 $(filename)
	latex $(filename)
	latex $(filename)

clean:
	rm -f $(filename)*.dvi
	rm -f $(filename)*.ps
	rm -f $(filename)*.pdf
	rm -f $(filename)*.bbl
	rm -f $(filename)*.aux
	rm -f $(filename)*.log
	rm -f $(filename)*.out

supp: supp.tex
	latex supp.tex
	# bibtex --min-crossrefs=20 supp
	latex supp.tex
	latex supp.tex
	dvips -Ppdf -t letter -G0 supp.dvi
	./llpdfus supp.ps
	acroread supp.pdf & 
