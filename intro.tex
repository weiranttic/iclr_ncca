﻿\vspace{-.25in}
\section{Introduction}
\vspace{-.05in}

A common task in data analysis is to reveal the common variability in multiple views of the same phenomenon, while suppressing view-specific noise factors.  Canonical correlation analysis (CCA)~\citep{Hotell36a} is a classical statistical technique that targets this goal. In CCA, linear projections of two random vectors are sought, such that the resulting low-dimensional vectors are maximally correlated. This tool has found widespread use in various fields, including
%meteorology \citet{Anderson58}, chemometrics \citet{Montanarella95},
recent application to natural language processing~\citep{Dhillon_11b}, speech recognition \citep{AroraLivesc13a}, genomics \citep{Witten09}, and cross-modal retrieval~\citep{gong2014improving}. %\klcomment{Changed this to only cite recent work, because it saves references and seems more relevant for the NIPS community.}
%to detect interesting phenomena shared by two data sets and to learn informative single-view features based on multi-view training data. Examples include

One of the shortcomings of CCA is its restriction to linear mappings, since many real-world multi-view datasets exhibit highly nonlinear relationships. To overcome this limitation, several extensions of CCA have been proposed for finding maximally correlated \emph{nonlinear} projections. In kernel CCA (KCCA) \citep{Akaho01a,Melzer_01a,BachJordan02a,Hardoon_04a}, these nonlinear mappings are chosen from two reproducing kernel Hilbert spaces (RKHS). In deep CCA (DCCA) \citep{Andrew_13a}, the projections are obtained from two deep neural networks that are trained to output maximally correlated vectors. Nonparametric CCA-type methods, which are not limited to specific function classes, include the alternating conditional expectations (ACE) algorithm \citep{Breiman85} and its extensions \citep{Balakr_12a,Makur_15a}. Nonlinear CCA methods are advantageous over linear CCA in a range of applications \citep{Hardoon_04a,
%Vinokour_03a,
%Dhillon_11b,Witten09,
Melzer_01a,Wang_15b}.  However, existing nonlinear CCA approaches are very computationally demanding, and are often impractical to apply on large data.

Interestingly, the problem of finding the most correlated nonlinear projections of two random variables has been studied by \citet{lancaster1958} and \citet{hannan1961}, long before the derivation of KCCA, DCCA and ACE. They characterized the optimal projections in the population setting, without restricting the solution to an RKHS or to have any particular parametric form. However, these theoretical results have not inspired practical algorithms.

In this paper, we revisit Lancaster's theory, and use it to devise a practical algorithm for \emph{nonparametric CCA} (NCCA). Specifically, we show that the solution to the nonlinear CCA problem can be expressed in terms of the singular value decomposition (SVD) of a certain operator, which is defined via the population density. Therefore, to obtain a practical method, we estimate the density from training data and use the estimate in the solution. The resulting algorithm reduces to solving an eigenvalue system with a particular kernel that depends on the joint distribution between the views.  While superficially similar to other eigenvalue methods, it is fundamentally different from them and in particular has crucial advantages over KCCA.  For example, unlike KCCA, NCCA does not involve computing the inverse of any matrix, making it computationally feasible on large data where KCCA (even using approximation techniques) is impractical.  We elucidate this and other contrasts in Sec.~\ref{sec:algorithm} below.  We show that NCCA achieves state-of-the art performance, while being much more computationally efficient than KCCA and DCCA.


%limited to either one-dimensional projections (like the ACE algorithm and its extensions \wwcomment{We need to be careful about this now.}) or to specific families of nonlinear mappings (determined by the user-chosen kernels in KCCA and by the network architecture in DCCA).  DCCA is extremely flexible but requires extensive tuning of network topology and other hyperparameters.

%In this paper, we derive a closed-form solution to the \emph{nonparametric CCA} (NCCA) problem in the population setting.  \kl{reword a bit, and other places, to account for other population derivations.} We do not constrain the projection functions to lie in an RKHS or to be of any particular parametric form. The solution relies on the population density; to obtain a practical method, we estimate the density from training data and use the estimate in the solution. The resulting algorithm reduces to solving an eigenvalue system with a particular kernel that depends on the joint distribution between the views.  While superficially similar to other eigenvalue methods, it is fundamentally different from them and in particular has crucial advantages over KCCA.  For example, unlike KCCA, NCCA does not involve computing the inverse of any matrix, making it computationally feasible on large data where KCCA (even using approximation techniques) is impractical.  We elucidate this and other contrasts in Sec.~\ref{sec:algorithm} below.  We show that NCCA achieves state-of-the art performance, while being much more computationally efficient than KCCA and DCCA. \kl{added some verbiage}
%\tmcomment{We'll refine this statement after the experiments section is complete. I'm being optimistic for now...}
%unconstrained \wwcomment{I suggest remove ``unconstrained'' because it is followed by ``problem'' and might be interpreted mistakenly as ``unconstrained optimization problem''}

%The proposed NCCA solution can be expressed in terms of the exponent of the \emph{point-wise mutual information} between the views, $S(\x,\y)=p(\x,\y)/(p(\x)p(\y))$. Specifically, we show that the maximally correlated $L$-dimensional projections of two random vectors $X$ and $Y$ are, respectively, the top $L$ left- and right-singular functions of the kernel $S(\x,\y)$. Thus, in contrast to KCCA, in which kernels are fixed by the user, our derivation suggests that $S(\x,\y)$ is the ``correct'' kernel to use (but note that here the kernel is used in a very different way from KCCA).  There is also a computational advantage to the proposed approach:  Given a set of training data $\{\x_i,\y_i\}_{i=1}^N$, the $N\times N$ matrix $\S_{ij}=S(\x_i,\y_i)$ can be readily constructed (\eg using nonparametric density estimation), whereas KCCA involves the inversion of $N\times N$ matrices. \klcomment{Not sure this par belongs here -- it throws in technical detail where the rest of the section is very general.}
 %to compute \wwcomment{rephrase this sentence?}, and can be readily estimated from the training data.

%The correlations between the dimensions of the projections (known as the canonical correlations) are the corresponding singular values

In certain situations, nonlinearity is needed for one view but not for the other. In such cases, it may be advantageous to constrain the projection of the second view to be linear. An additional contribution of this paper is the derivation of a closed-form solution to this \emph{partially linear CCA} (PLCCA) problem in the population setting. We show that PLCCA has essentially the same form as linear CCA, but with the optimal linear predictor term in CCA replaced by an optimal nonlinear predictor in PLCCA. Thus, moving from the population setting to sample data entails simply using nonlinear regression to estimate this predictor. The resulting algorithm is efficient and, as we demonstrate on realistic data, sometimes matches NCCA and significantly outperforms CCA and KCCA.
%\tmcomment{We'll refine after the experiments section is complete.}

In the following sections we review CCA and nonlinear extensions (Sec.~\ref{sec:backgound}), derive NCCA and PLCCA in the population setting and provide empirical algorithms (Sec.~\ref{sec:algorithm}), discuss related work (Sec.~\ref{sec:related}), and compare our algorithms to CCA/KCCA/DCCA experimentally on both synthetic and real-world data (Sec.~\ref{s:expt}).



%$S(\x,\y)=\bbP(\x,\y)/(\bbP(\x)\bbP(\y))$


%Both CCA and KCCA are techniques for learning representations
%of two data views, such that each view’s
%representation is simultaneously the most predictive of,
%and the most predictable by, the other.
%CCA and KCCA have been used for unsupervised data
%analysis when multiple views are available (Hardoon
%et al., 2007; Vinokourov et al., 2003; Dhillon et al.,
%2011); learning features for multiple modalities that are
%then fused for prediction (Sargin et al., 2007); learning
%features for a single view when another view is
%available for representation learning but not at prediction
%time (Blaschko & Lampert, 2008; Chaudhuri
%et al., 2009; Arora & Livescu, 2012); and reducing sample
%complexity of prediction problems using unlabeled
%data (Kakade & Foster, 2007). The applications range
%broadly across a number of fields, including medicine,
%meteorology (Anderson, 1984), chemometrics (Montanarella
%et al., 1995), biology and neurology (Vert &
%Kanehisa, 2002; Hardoon et al., 2007), natural language
%processing (Vinokourov et al., 2003; Haghighi et al.,
%2008; Dhillon et al., 2011), speech processing (Choukri
%& Chollet, 1986; Rudzicz, 2010; Arora & Livescu, 2013),
%computer vision (Kim et al., 2007), and multimodal
%signal processing (Sargin et al., 2007; Slaney & Covell,
