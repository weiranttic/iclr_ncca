# Useful: grep "@" nips15a.bib | gawk -F "{" '{ print $2 }'  | gawk -F "," '{ print $1 }' | grep "^[A-Z]" | paste -s -d "|"

tmp1=$$.tmp1
tmp2=$$.tmp2
tmp3=$$.tmp3

~/bibextract/bibextract.sh "" $(cat $1) ./macp.bib ./macp-xref.bib > $tmp1
sed -i -e '1d' -e "/bibsource/d" -e "/reviewed/d" -e "/address/d" -e "/editor/d" -e "/month/d" -e "/publisher/d" $tmp1
grep "crossref" $tmp1 | gawk -F '=' '{ print $2 }' | gawk -F '"' '{ print $2 }' > $tmp2
cat $tmp1
~/bibextract/bibextract.sh "" $(paste -s -d '|' $tmp2) ./macp-xref.bib > $tmp3
sed -i -e '1d' -e "/bibsource/d" -e "/reviewed/d" -e "/address/d" -e "/editor/d" -e "/month/d" -e "/publisher/d" $tmp3
cat $tmp3

rm $tmp1 $tmp2 $tmp3
